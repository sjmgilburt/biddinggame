/** COMP3211 Project
 *  Author: Samuel Gilburt
 *  Student No.: 20254712
 *  Date: 19 April 2015
 *  Version: 1.0
 */

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

public class Bidder {
	
	// GLOBAL VARIABLES ---------------------------------------------------------------------------
	private static int 	bidderNo;
	private static boolean random = false;
	private static int 	auctionNo;
	private static boolean allBidsAnnounced = false;
	private static int bidRule = 0;
	/*
	private static BidRule	bidRule; - REPLACED due to compilation complications.
	Key:
		1: ALL
		2: PRISONER
		3: ONENASH
		4: TWONASH
		3: HTHOUSANDTH
	*/
	
	private static File myFile = new File("my_file1_20254712");
	private static int lastRoundNo = 0;
	private static double cumulWins = 0;
	
	/*
	public enum BidRule {
	
		ALL, PRISONER, ONENASH, TWONASH, HTHOUSANDTH
	}
	*/
	
	// READERS & WRITERS --------------------------------------------------------------------------
	private static void readSetup() {
		try {
			File setupFile = new File("setup.txt");
			FileReader fileReader = new FileReader(setupFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			List lines = new ArrayList();
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				lines.add(line);
			}
			fileReader.close();
			
			//System.out.println(lines.toString()); //test
			
			bidderNo = Integer.parseInt((String)lines.get(0));
			random = lines.get(1).equals("1");
			auctionNo = Integer.parseInt((String)lines.get(2));
			allBidsAnnounced = lines.get(3).equals("1");
			
			String ruleLine = (String)lines.get(4);
			if (ruleLine.equals("[0,1]")) {
				bidRule = 1;
			}
			else if (ruleLine.equals("{0.3,0.6}")) {
				bidRule = 2;
			}
			else if (ruleLine.equals("{0.3,0.6,0.7}")) {
				bidRule = 3;
			}
			else if (ruleLine.equals("{0.3,0.6,0.9}")) {
				bidRule = 4;
			}
			else if (ruleLine.equals("{0,1/100000,...,99999/100000,1}")) {
				bidRule = 5;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private static String[][] readLastTopTwo() {
		try {
			File resultFile = new File("result.txt");
			FileReader fileReader = new FileReader(resultFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			//List<String> lines = new ArrayList<String>();
			String searchLine;
			String[][] lastRound = new String[2][2];
			while ((searchLine = bufferedReader.readLine()) != null) {
				if (searchLine.equals("round " + lastRoundNo)) {
					String winLine = bufferedReader.readLine();
					StringTokenizer tokenizer = new StringTokenizer(winLine, ", ");
					lastRound[0][0] = tokenizer.nextToken();
					lastRound[0][1] = tokenizer.nextToken();
					
					if (allBidsAnnounced) {
						String secondLine = bufferedReader.readLine();
						tokenizer = new StringTokenizer(secondLine, ", ");
						lastRound[1][0] = tokenizer.nextToken();
						lastRound[1][1] = tokenizer.nextToken();
					}
					break;
				}
			}
			fileReader.close();
			
			//System.out.println(lastRound); //test
			
			return lastRound;
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static void writeMyFile() {
		try {
			myFile.delete();
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(myFile));
		
			double[] store = new double[2];
			store[0] = lastRoundNo;
			store[1] = cumulWins;
			out.writeObject(store);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private static void readMyFile() {
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(myFile));
			
			double[] store = new double[2];
			store = (double[])in.readObject();
			lastRoundNo = (int)store[0];
			cumulWins = store[1];
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	// BIDDERS ------------------------------------------------------------------------------------
	private static double bid() {
		// ALL & HTHOUSANTH
		if ((bidRule==1) || (bidRule==5)) {
			if ((bidderNo==2) && random) return 0.5;
			
			if (lastRoundNo == 0) return 0.0;
			
			String[][] previous = readLastTopTwo();
			if ((previous[1][1] != null) && previous[1][1].equals("-")) return 0.0;
			double lastWin = 0.501;
			double lastLoss = 0;
			boolean iWon = true;
			if ((previous[0][0] != null) && (previous[0][1] != null) && (previous[1][1] != null)) {
				iWon = previous[0][0].equals("20254712");
				lastWin = Double.parseDouble(previous[0][1]);
				lastLoss = Double.parseDouble(previous[1][1]);
			}
			
			cumulWins += lastWin;
			
			if (lastLoss==-1) return 0.0;
			
			double avgWin = cumulWins/lastRoundNo;
			
			Random rand = new Random();
			double bid;
			if (iWon) {
				bid = lastWin - rand.nextDouble()*((lastWin-lastLoss)/2);
			}
			else {
				bid = avgWin + rand.nextDouble()*((lastWin-lastLoss));
			}
			
			if (bid >= 1.0) return 0.501;
			return Math.floor(bid * 100000) / 100000;
		}
		
		// PRISONER
		if (bidRule==2) {
			if ((bidderNo!=2) || random) return 0.6;
			
			if (lastRoundNo == 0) return 0.3;
			
			String[][] previous = readLastTopTwo();			
			if ((previous[1][1] != null) && previous[1][1].equals("-")) return 0.3;
			double oppbid = 0.6;
			if ((previous[0][1] != null) && (previous[1][1] != null)) {
				if ((previous[0][0].equals("20254712"))) {
					oppbid = Double.parseDouble(previous[1][1]);
				}
				else oppbid = Double.parseDouble(previous[0][1]);
			}
			
			if (oppbid==-1) return 0.3;

			return oppbid;
		}
		
		// ONENASH
		if (bidRule==3) {
			if ((bidderNo!=2) || random) return 0.7;
			
			if (lastRoundNo==0) return 0.3;
		
			String[][] previous = readLastTopTwo();
			if ((previous[1][1] != null) && previous[1][1].equals("-")) return 0.3;
			double mybid = 0.7;
			double oppbid = 0.7;
			if ((previous[0][1] != null) && (previous[1][1] != null)) {
				if ((previous[0][0].equals("20254712"))) {
					mybid = Double.parseDouble(previous[0][1]);
					oppbid = Double.parseDouble(previous[1][1]);
				}
				else {
					mybid = Double.parseDouble(previous[1][1]);
					oppbid = Double.parseDouble(previous[0][1]);
				}
			}
			
			if (oppbid==-1) return 0.3;
			if ((mybid==0.7) || (oppbid==0.7)) return 0.7;

			return oppbid;
		}
		
		//TWONASH
		if (bidRule==4) {
			if (bidderNo!=2) return 0.9;
			if (random) return 0.6;
			
			if (lastRoundNo==0) return 0.3;
			
			String[][] previous = readLastTopTwo();
			if ((previous[1][1] != null) && previous[1][1].equals("-")) return 0.3;
			double mybid = 0.9;
			double oppbid = 0.9;
			if ((previous[0][1] != null) && (previous[1][1] != null)) {
				if ((previous[0][0].equals("20254712"))) {
					mybid = Double.parseDouble(previous[0][1]);
					oppbid = Double.parseDouble(previous[1][1]);
				}
				else {
					mybid = Double.parseDouble(previous[1][1]);
					oppbid = Double.parseDouble(previous[0][1]);
				}
			}
			
			if (oppbid==-1) return 0.3;
			if ((mybid==0.9) || (oppbid==0.9)) return 0.9;

			return oppbid;
		}
		
		return 0.6; // default bid
	}
	
	
	// MAIN ---------------------------------------------------------------------------------------
	public static void main(String [ ] args) {
		readSetup();
		if (myFile.exists()) readMyFile();
		double bid = bid();
		
		lastRoundNo++;
		writeMyFile();
		
		System.out.println(bid);
	}
	
}
