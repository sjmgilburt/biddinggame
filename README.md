# Bidding Game

This project was completed in April 2015 by Samuel J. M. Gilburt as part of an assignment for the CSC3211 Fundamentals 
of Artificial Intelligence course at Hong Kong University of Science and Technology (HKUST).

The main component of the project is a Bidder class, which is an AI designed to take part in a simple bidding game 
against other AIs, with the aim of winning as many auction rounds as possible without unduly sacrificing profit. It is 
loosely based on the so-called ["Dollar Auction"](https://en.wikipedia.org/wiki/Dollar_auction) game. The full 
specification for the assignment (and the server-side auction program) can be found 
[here](https://en.wikipedia.org/wiki/Dollar_auction).

## Strategies

What follows is an explanation of the strategies chosen for dealing with the different types of games available. For 
some, there are known optimal strategies. For others, the AI attempts to outsmart its fellow bidders.

### [0,1] or {0,1/100000,...,99999/100000,1}

The bidder treats both the above cases as the same. If it is pairwise against RANDOM, it will continually bid 0.5 as 
this is optimal. If it is the first round, it starts at 0.0 in an attempt to cooperate with the opponent/reduce the 
initial starting point of bids so that it does not rise too quickly. Otherwise, the algorithm runs like so:
1. Read the last top two results from the result.txt file
2. Check if won
3. The bidder keeps a cumulative store of the winning bids, so that this can be used to calculate the average win 
value.
    * If won the last round, the next bid is a reduction of the previous winning bid by some random value to between 
    said previous bid and halfway down to the previous first losing bid (the opponent's first bid)
    * If did not win the last round, the next bid is the average bid plus some random value between 0 and the 
    difference between the previous winning bid and the previous first losing bid
4. If the next bid calculated is >= 1, a default bid 0.501 is returned

This results in my bid increasing if the bidder did not win the last round, and decreasing slightly if it did win the #
last round (in order to try and maximise profit). The element of randomness added is an attempt to trick opposition 
bidders that may be attempting to predict the next bid. I decided to take an approach independent of predicting other 
bidders.

### {0.3,0.6} (Prisoner's Dilemma)

For the Prisoner’s Dilemma, the bidder uses a simple tit-for-tat algorithm. Starting low (in an attempt to cooperate 
with the opponent), it then copies exactly the opponent’s previous bid. If it is against RANDOM, it always bids 0.6.

### {0.3,0.6,0.7} (exactly one Nash equilibrium)

For the single Nash equilibrium game, the bidder starts low again, attempting to collaborate with the opponent. 
However, if this does not work, the function rises to 0.7 and stays there. If it is against more than one opponent or 
RANDOM, it always bids 0.7.

### {0.3,0.6,0.9} (two Nash equilibria)

The double Nash equilibrium game is very much the same as the single Nash – gradually rising in an attempt to cooperate 
but remaining at 0.9 if it does not work. If it is against multiple opponents it always bids 0.9; if it is against 
RANDOM it always bids 0.6.

## Acknowledgements

* [Romzicon](https://thenounproject.com/romzicon) - project icon
